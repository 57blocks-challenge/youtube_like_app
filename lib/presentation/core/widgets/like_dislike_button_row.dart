import 'package:flutter/material.dart';
import 'package:youtube_like_app/domain/video/video.dart';
import 'package:youtube_like_app/presentation/core/widgets/like_dislike_button.dart';

class LikeDislikeRow extends StatelessWidget {
  const LikeDislikeRow({
    Key key,
    @required Video video,
    @required Function likePressed,
    @required Function dislikePressed,
  })  : _video = video,
        _likePressed = likePressed,
        _dislikePressed = dislikePressed,
        super(key: key);

  final Video _video;
  final Function _likePressed;
  final Function _dislikePressed;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        LikeDislikeButton(
          iconData: Icons.thumb_up,
          colored: _video.liked,
          pressed: () => _likePressed(),
        ),
        LikeDislikeButton(
          iconData: Icons.thumb_down,
          colored: _video.disliked,
          pressed: () => _dislikePressed(),
        ),
      ],
    );
  }
}
