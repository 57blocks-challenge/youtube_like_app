import 'package:flutter/material.dart';

class LikeDislikeButton extends StatelessWidget {
  const LikeDislikeButton({
    Key key,
    @required Function pressed,
    @required IconData iconData,
    @required bool colored,
  })  : _pressed = pressed,
        _iconData = iconData,
        _colored = colored,
        super(key: key);

  final bool _colored;
  final Function _pressed;
  final IconData _iconData;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        _iconData,
        color: _colored ? Colors.blueAccent : Colors.grey,
      ),
      onPressed: () {
        _pressed();
      },
    );
  }
}
