import 'package:flutter/material.dart';
import 'package:youtube_like_app/domain/video/video.dart';
import 'package:youtube_like_app/presentation/core/widgets/like_dislike_button_row.dart';

class DetailPage extends StatelessWidget {
  final Video _video;

  DetailPage(Video video) : _video = video;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Video Detail"),
      ),
      body: Column(
        children: <Widget>[
          Image(image: AssetImage('assets/images/video_mock.png')),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(_video.name),
                LikeDislikeRow(
                  video: _video,
                  dislikePressed: () {},
                  likePressed: () {},
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
