import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_like_app/domain/video/video.dart';
import 'package:youtube_like_app/presentation/core/widgets/like_dislike_button_row.dart';
import 'package:youtube_like_app/presentation/core/widgets/points_loader.dart';

class HomePage extends StatelessWidget {
  final List<Video> _videos;
  final bool _isLoading;
  final void Function() _requestNext;
  final void Function(int itemIndex) _likePressed;
  final void Function(int itemIndex) _dislikePressed;
  final void Function(int itemIndex) _videoSelected;

  HomePage({
    @required List<Video> videos,
    @required bool isLoading,
    @required requestNext,
    @required likePressed,
    @required dislikePressed,
    @required videoSelected,
  })  : _videos = videos,
        _isLoading = isLoading,
        _requestNext = requestNext,
        _likePressed = likePressed,
        _dislikePressed = dislikePressed,
        _videoSelected = videoSelected;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _videos.length + (_isLoading ? 1 : 0),
      itemBuilder: (context, index) {
        if (index == _videos.length - 2) {
          _requestNext();
        }

        return _isLoading && index == _videos.length
            ? PointsLoader.random()
            : ListTile(
                title: Text(_videos.elementAt(index).name),
                onTap: () => _videoSelected(index),
                trailing: LikeDislikeRow(
                    video: _videos.elementAt(index),
                    likePressed: () => _likePressed(index),
                    dislikePressed: () => _dislikePressed(index)),
              );
      },
    );
  }
}
