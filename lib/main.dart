import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:youtube_like_app/application/home/home_page_bloc.dart';
import 'package:youtube_like_app/infrastructure/video/video_repository.dart';
import 'package:youtube_like_app/presentation/detail/detail_page.dart';
import 'package:youtube_like_app/presentation/home/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Youtube Like App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider<HomePageBloc>(
        create: (context) =>
            HomePageBloc(VideoRepository())..add(HomePageLoadVideoRequested()),
        child: MyHomePage(title: 'Youtube Like App'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static _createHomePage() => BlocConsumer<HomePageBloc, HomePageState>(
      listener: (context, state) {
        if (state.selectedVideo == null) {
          return;
        }
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailPage(state.selectedVideo)));
        BlocProvider.of<HomePageBloc>(context).add(HomePageVideoShown());
      },
      builder: (context, state) => HomePage(
            isLoading: state.isLoading,
            videos: state.videos,
            requestNext: () {
              BlocProvider.of<HomePageBloc>(context)
                  .add(HomePageLoadVideoRequested());
            },
            likePressed: (int index) {
              BlocProvider.of<HomePageBloc>(context)
                  .add(HomePageVideoLiked(index));
            },
            dislikePressed: (int index) {
              BlocProvider.of<HomePageBloc>(context)
                  .add(HomePageVideoDisliked(index));
            },
            videoSelected: (int index) {
              BlocProvider.of<HomePageBloc>(context)
                  .add(HomePageVideoSelected(index));
            },
          ));

  int _selectedIndex = 0;

  _onItemTapped(currentIndex) {
    setState(() {
      _selectedIndex = currentIndex;
    });
  }

  final pages = [
    _createHomePage(),
    Container(
      color: Colors.red,
    ),
    Container(
      color: Colors.blueAccent,
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.subscriptions),
            title: Text("Subscriptions"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text("Profile"),
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
      body: pages.elementAt(_selectedIndex),
    );
  }
}
