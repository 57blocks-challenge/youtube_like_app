import 'package:youtube_like_app/domain/video/video.dart';

class VideoRepository {
  Future<List<Video>> getNextVideos(int indexToStart) async {
    await Future.delayed(Duration(seconds: 2));

    final List<Video> newVideos = [];

    for (int i = indexToStart; i < indexToStart + 15; i++) {
      final newVideo = Video(name: "Video $i");
      newVideos.add(newVideo);
    }

    return newVideos;
  }
}
