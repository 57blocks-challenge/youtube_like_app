part of 'home_page_bloc.dart';

@immutable
class HomePageState {
  final List<Video> _videos;
  final bool isLoading;
  final Video selectedVideo;

  List<Video> get videos => List.unmodifiable(_videos);

  HomePageState copyWith(
      {List<Video> videos, bool isLoading, Video selectedVideo}) {
    final newVideos = videos == null ? _videos : videos;
    final newIsLoading = isLoading == null ? this.isLoading : isLoading;

    return HomePageState(
        videos: newVideos,
        isLoading: newIsLoading,
        selectedVideo: selectedVideo);
  }

  HomePageState(
      {List<Video> videos = const [],
      this.isLoading = false,
      this.selectedVideo})
      : this._videos = videos;
}

class HomePageInitial extends HomePageState {}
