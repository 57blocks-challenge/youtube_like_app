part of 'home_page_bloc.dart';

@immutable
abstract class HomePageEvent {}

class HomePageVideoLiked extends HomePageEvent {
  final int videoIndex;

  HomePageVideoLiked(this.videoIndex);
}

class HomePageVideoDisliked extends HomePageEvent {
  final int videoIndex;

  HomePageVideoDisliked(this.videoIndex);
}

class HomePageLoadVideoRequested extends HomePageEvent {}

class HomePageVideoSelected extends HomePageEvent {
  final int videoIndex;

  HomePageVideoSelected(this.videoIndex);
}

class HomePageVideoShown extends HomePageEvent {
  HomePageVideoShown();
}
