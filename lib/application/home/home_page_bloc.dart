import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:youtube_like_app/domain/video/video.dart';
import 'package:youtube_like_app/infrastructure/video/video_repository.dart';

part 'home_page_event.dart';
part 'home_page_state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  final VideoRepository _videoRepository;

  HomePageBloc(this._videoRepository) : super(HomePageInitial());

  @override
  Stream<HomePageState> mapEventToState(
    HomePageEvent event,
  ) async* {
    if (event is HomePageLoadVideoRequested) {
      yield* _mapLoadVideoRequestedToState(event);
    } else if (event is HomePageVideoLiked) {
      yield* _mapVideoLikedToState(event);
    } else if (event is HomePageVideoDisliked) {
      yield* _mapVideoDislikedToState(event);
    } else if (event is HomePageVideoSelected) {
      yield* _mapVideoSelectedToState(event);
    } else if (event is HomePageVideoShown) {
      yield* _mapVideoShownToState(event);
    }
  }

  Stream<HomePageState> _mapLoadVideoRequestedToState(
      HomePageLoadVideoRequested event) async* {
    yield state.copyWith(isLoading: true);

    final newVideos = await _videoRepository.getNextVideos(state.videos.length);

    final videos = [...state.videos, ...newVideos];

    yield state.copyWith(videos: videos, isLoading: false);
  }

  Stream<HomePageState> _mapVideoLikedToState(HomePageVideoLiked event) async* {
    final videoIndex = event.videoIndex;
    final newVideos = List.of(state._videos);

    final newVideo = newVideos.elementAt(videoIndex).toggleLiked();
    newVideos[videoIndex] = newVideo;

    yield state.copyWith(videos: newVideos);
  }

  Stream<HomePageState> _mapVideoDislikedToState(
      HomePageVideoDisliked event) async* {
    final videoIndex = event.videoIndex;
    final newVideos = List.of(state._videos);

    final newVideo = newVideos.elementAt(videoIndex).toggleDisliked();
    newVideos[videoIndex] = newVideo;

    yield state.copyWith(videos: newVideos);
  }

  Stream<HomePageState> _mapVideoSelectedToState(
      HomePageVideoSelected event) async* {
    final selectedVideo = state._videos.elementAt(event.videoIndex);
    yield state.copyWith(selectedVideo: selectedVideo);
  }

  Stream<HomePageState> _mapVideoShownToState(HomePageVideoShown event) async* {
    yield state.copyWith(selectedVideo: null);
  }
}
