class Video {
  final String name;
  final bool liked;
  final bool disliked;

  const Video({this.name = "", this.liked = false, this.disliked = false});

  Video copyWith({String name, bool liked = false, disliked = false}) {
    final newName = name == null ? this.name : name;
    final newLiked = liked == null ? this.liked : liked;
    final newDisliked = disliked == null ? this.disliked : disliked;

    return Video(name: newName, liked: newLiked, disliked: newDisliked);
  }

  Video toggleLiked() => copyWith(liked: !liked);

  Video toggleDisliked() => copyWith(disliked: !disliked);
}
